//---------------------------------------------------------------------------
#ifndef MYMATRIX_H
#define MYMATRIX_H

#include <vector>
#include <iomanip>

using namespace std;

/**
 * Class template representing a matrix with integer indices a parameterized value type 
 * The implementation uses the std::vector class
 */
template<typename VALUE_TYPE>
class Matrix   {
public:
	/**
	*@param aRows Number of rows
	*@param aCols Number of columns
	*@param aInitVal Initial cell value
	*/
	Matrix(unsigned int aRows, unsigned int aCols, VALUE_TYPE aInitVal=0)
	:nRows(aRows)
	{
		iRows.resize(nRows);
		// resize each row vector
		for (unsigned int i = 0; i < nRows; ++i) {
			iRows[i].resize(aCols);
			// Assign default values
			for (unsigned int j = 0; j < aCols; ++j)
				iRows[i][j] = aInitVal;
		}
	}

	vector<VALUE_TYPE>& operator[](unsigned int idx) { return iRows[idx]; }
	const vector<VALUE_TYPE>& operator[](unsigned int idx) const { return iRows[idx]; }

	unsigned int nRow() const { return iRows.size(); }
	unsigned int nCol() const { return iRows[0].size(); }

private:
		vector<vector<VALUE_TYPE>> iRows;
        unsigned int nRows;
};  

#endif
