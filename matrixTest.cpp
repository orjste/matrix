#include<iostream>
#include<stdexcept>
#include "Matrix.hpp"

using std::cout;
using std::endl;

/**
* Transpones a matrix: first row becomes first column etc.
* @param orig The original matrix
* @param transponed The transponed matrix
*/
template<typename T>
void transpone(const Matrix<T> &orig, Matrix<T> &transponed) {	
	for (int row = 0; row < orig.nRow(); ++row)
		for (int col = 0; col < orig.nCol(); ++col)
			transponed[col][row] = orig[row][col];
}

/**
* Prints a matrix on cout
* @param mat The matrix to print
*/
template<typename T>
void printMatrix(const Matrix<T> &mat) {
	for (int i = 0; i < mat.nRow(); ++i) {
		for (int j = 0; j < mat.nCol(); ++j) {
			T val = mat[i][j];
			cout << std::setw(2) << std::setfill('0') <<  val << ' ';
		}
		cout << '\n';
	}
	cout << endl;
}

/**
* Multiplies each element in the matrix with a scalar.
* @param orig The original matrix
* @param result The multilpied matrix
* @scalar The multiplication factor
*/
template<typename T,typename V>
void scalarMul(const Matrix<T> &orig, Matrix<T> &result, V scalar) {
	for (int row = 0; row < orig.nRow(); ++row)
		for (int col = 0; col < orig.nCol(); ++col)
			result[row][col] = orig[row][col]*scalar;
}


int main(int argc, char* argv[])
{
  Matrix<double> mat(10,10);

  for(int i=0; i<10; ++i) {
    for(int j=0; j<10; ++j) {
		mat[i][j] = 10 * i + j;
    }
  }

  printMatrix(mat); // Original

  Matrix<double> mat2(10, 10, 0);
  transpone(mat, mat2);  // Transposed
  printMatrix(mat2);
  scalarMul(mat2, mat, 10.0); // Original multiplied
  printMatrix(mat);




  system("pause");
  return 0;
}
//---------------------------------------------------------------------------
 